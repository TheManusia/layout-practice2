import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final imageSize = 100.0;
    final imageFrame = ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: Image.asset(
        "images/dummy.jpg",
        height: imageSize,
        width: imageSize,
      ),
    );

    final name = Container(
      padding: EdgeInsets.only(top: 8),
      child: Text(
        "Rizqi Audianto Fari",
        style: TextStyle(
            fontWeight: FontWeight.w500, color: Colors.black, fontSize: 20),
      ),
    );

    final role = Container(
      padding: EdgeInsets.only(bottom: 8),
      child: Text(
        "Student",
        style: TextStyle(
            fontWeight: FontWeight.w500, color: Colors.blue, fontSize: 15),
      ),
    );

    Widget infoItem({title, value}) => Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Text(
                title,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
              ),
            ),
            Container(
              width: 100,
              color: Colors.transparent,
              child: Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Center(
                    child: Text(
                      value,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 15),
                    ),
                  )),
            ),
          ],
        );

    final info = Container(
      padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          infoItem(title: "Height", value: "185 cm"),
          infoItem(title: "Weight", value: "65 kg"),
          infoItem(title: "Age", value: "21"),
        ],
      ),
    );

    Widget recapItem(
            {required String title,
            required double min,
            required double max,
            required String info}) =>
        Container(
          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        "${min.toInt()}",
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.black),
                      ),
                      Text(
                        "/${max.toInt()} $info",
                        style: TextStyle(
                          color: Colors.black38,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: LinearPercentIndicator(
                  lineHeight: 14.0,
                  percent: (min / max),
                  backgroundColor: Colors.black12,
                  progressColor: Colors.blue,
                ),
              )
            ],
          ),
        );

    final recapItems = Container(
        padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: Column(
          children: [
            recapItem(title: "Sleep", min: 100, max: 300, info: "hours"),
            recapItem(title: "Running", min: 20, max: 100, info: "km"),
            recapItem(title: "Lose Weight", min: 3, max: 6, info: "kilos"),
          ],
        ));

    final recap = Container(
      padding: EdgeInsets.only(top: 16),
      child: Column(children: [
        Container(
          alignment: Alignment.centerLeft,
          child: Text(
            "Monthly Recap",
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
          ),
        ),
        recapItems
      ]),
    );

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[imageFrame, name, role, info, recap],
          ),
        ),
      ),
    );
  }
}
